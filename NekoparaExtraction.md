# Nekopara Data Extraction for Vol. 1, 0, 2, 3, 4, and Extra
- This guide can also be used in other VNs that have the Kirikiri/CatSystem2 engine

## Tools Needed:
-	[KrkrExtract](https://github.com/xmoeproject/KrkrExtract) (for extraction of .xp3 files)
-	[AlphaMovieDecoder](https://github.com/xmoeproject/AlphaMovieDecoder) (for .amv files)
-	[FreeMote](https://github.com/UlyssesWu/FreeMote) (for E-mote .psb files)
-	[KrKrZSceneManager](https://github.com/marcussacana/KrKrZSceneManager) (for .scn files)
-	[GARbro](https://github.com/morkt/GARbro) (for extraction of .int files, Vol. 3 only)
-	[CatSceneEditor](https://github.com/marcussacana/CatSceneEditor) (for .cstl files, Vol. 3 only)

## Usage of KrkrExtract (extraction of assets for all games except Vol. 3)
- Note: old version (1.0.3.1) is used here, not sure if some of the steps here are needed for the newer version
- Prerequisites: You need the KrkrExtract .exe and .dll. If on Windows 10, disable Windows Defender Virus & Threat Protection first BEFORE downloading! You can find this on: Windows Defender Security Center > Settings > Virus & Threat Protection Settings > Turn Off Real-time Protection
- You may also add an exception to KrkrExtract.exe in Exclusions (also in Virus & Threat Protection settings)
1. Put the KrkrExtract .exe and .dll in the game directory
2. Drag the game .exe to KrkrExtract.exe. This should cause the game to open along with KrkrExtract
3. Check Full Unpack (you can skip this if it crashes)
4. Drag the .xp3 files to the KrkrExtract window, there will be a new folder called outPath with the extracted files
- You may repeat step 4 on all .xp3 files

## E-mote models (for all games)
- Note: Nekopara Extra is the best source for the E-mote models, as it contains all of the models used in all Nekopara games (this may have changed now with Vol. 4, but I can't check yet)
1. Extract the contents of emotewin.xp3 using KrkrExtract (replace emotewin.xp3 with emotedx.xp3 for Extra, psz.int for Vol. 3 using GARbro)
2. You can now use EmtConvert from FreeMoteToolkit on a cmd window to convert the .psb's to pure .psb's (this step is not needed for Extra)
- Usage: `EmtConvert.exe <drag PSB/PSZ file> 742877301`
- You may repeat step 2 on all other .psb's
- Pure .psb's can be used on the other tools in the FreeMoteToolkit. One usage is to view it with the FreeMoteViewer by dragging the pure .psb to FreeMoteViewer.exe

## Scene/script files (for all games except Vol. 3)
1. Extract the contents of data.xp3 using KrkrExtract
2. Go to outPath/data.xp3/scn
3. You can now open the .tjs file using a text editor, or open the .scn file using KrKrZSceneManager (ScnEditorGUI)

## Opening .amv files (for all games except Vol. 3)
- Note: old version is used here (AlphaMovieDecoderFakeOld); KrkrExtract also seems to have a built-in amv decoder now on the latest version
1. Most if not all .amv files are in adultsonly.xp3 so extract that first with KrkrExtract
2. You can now use AlphaMovieDecoder on a cmd window to extract the frames of the .amv files
Usage: `AlphaMovieDecoderFake.exe -amvpath=<drag AMV file>`

## Usage of GARbro (extraction of assets for Vol. 3 only)
1. Open the .int file in GARbro
2. You can now extract everything by selecting all, right-click, extract
3. You can set up the extraction with the options: Extract images, Extract audio, Convert audio to common format, and Save images as your preferred format (PNG is recommended)
4. You can now repeat these steps to all other .int files
- For animated scenes in image_ev.int, you need to double-click the .hg3 (eg. CG3_04c_anime.hg3) and you can now extract all the frames in there

## Scene/script files (for Vol. 3 only)
1. Extract the contents of scene.int using GARbro
2. You can now open the .cstl files in CatSceneEditor (CatSystemEditor)